<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Workshop</title>
</head>
<body>
    <div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
      Usuarios
        </div>
        <div class="panel-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                      <th>usuario</th>
                      <th>pass</th>
                      <th>tipo</th>
                      <th>nombre</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    include 'archivo.php';
                    $query = $db->query("SELECT * FROM usuarios ORDER BY id DESC");
                    if($query->num_rows > 0){ 
                        while($row = $query->fetch_assoc()){ ?>                
                    <tr>
                      <td><?php echo $row['usuario']; ?></td>
                      <td><?php echo $row['pass']; ?></td>
                      <td><?php echo $row['tipo']; ?></td>
                      <td><?php echo $row['nombre']; ?></td>
                    </tr>
                    <?php } }else{ ?>
                    <tr><td colspan="5">No se encontraon usuarios</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>